import java.util.ArrayList;
import java.util.List;

import com.zc.class2json.trans.main.DataClass2Json;
import com.zc.dataclass.main.PersonData;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestJar1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<PersonData> listPD = getPersonData();
		DataClass2Json dcj = new DataClass2Json();
		JSONArray jsonArr = dcj.data2JsonArr(listPD);
		System.out.println(jsonArr.toString());
		//System.out.println(dcj.data2JsonObj(listPD).toString());
		
		PersonData pd1 = new PersonData();
		pd1.setXm("张三");
		pd1.setXb("男");
		pd1.setSr("1988-07-20");
		JSONObject jsonObj = dcj.data2JsonObj(pd1);
		System.out.println(jsonObj.toString());
		
		JSONArray jsonArr1 = dcj.data2JsonArr(pd1);
		System.out.println(jsonArr1.toString());
	}
	
	/*
	 * 获取类数据
	 */
	public static List<PersonData> getPersonData() {
		PersonData pd1 = new PersonData();
		pd1.setXm("李四");
		pd1.setXb("男");
		pd1.setSr("1991-01-12");
		
		PersonData pd2 = new PersonData();
		pd2.setXm("张楠");
		pd2.setXb("女");
		pd2.setSr("1995-05-10");
		
		List<PersonData> pd = new ArrayList<PersonData>();
        pd.add(pd1);
        pd.add(pd2);
        //System.out.println("");
        
        return pd;
	}

}
