package com.zc.class2json.trans.main;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @author zhangchao
 * @date 2018-08-22
 * 将数据类转换成Json格式数据
 */
public class DataClass2Json {
	public JSONObject data2JsonObj(Object obj) {
		JSONObject jsonObj = JSONObject.fromObject(obj); 
		return jsonObj;
	}
	
	/* 错误的例子
	public JSONObject data2JsonObj(List<Object> list) {
		JSONObject jsonObj = JSONObject.fromObject(list); 
		return jsonObj;
	}*/
	
	public JSONArray data2JsonArr(Object obj) {
		JSONArray jsonArr = JSONArray.fromObject(obj); 
		return jsonArr;
	}
	
	public JSONArray data2JsonArr(List<Object> list) {
		JSONArray jsonArr = JSONArray.fromObject(list); 
		return jsonArr;
	}
	
	/*public JSONArray data2JsonArr2(List<Object> list) {
		JSONArray json = new JSONArray();
        for(Object pLog : list){
            JSONObject jo = new JSONObject();
            jo.put("id", pLog.getId());
            jo.put("time", pLog.getBeginTime());
             
            // 注意add和element的区别
            json.add(jo);
        }
        return json;
	}*/
}
