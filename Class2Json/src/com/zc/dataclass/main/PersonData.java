package com.zc.dataclass.main;

import java.io.Serializable;

public class PersonData implements Serializable {
	private static final long serialVersionUID = 1L;
	
    private String xm;  //姓名
    private String xb;  //性别
    private String sr;  //生日
	public String getXm() {
		return xm;
	}
	public void setXm(String xm) {
		this.xm = xm;
	}
	public String getXb() {
		return xb;
	}
	public void setXb(String xb) {
		this.xb = xb;
	}
	public String getSr() {
		return sr;
	}
	public void setSr(String sr) {
		this.sr = sr;
	}
    
}
